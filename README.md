# DiceRoller

## About

This simple bot parses expressions containing dice rolls such as `2d6` or
`d20`, and computes the result.

## Installation

This bot uses [discord.js](https://www.npmjs.com/package/discord.js) v13,
which requires Node 14.x.

Run `npm install`, then create a `src/config.json` file containing your bot token.
You can look into `src/config.example.json` for an example.

Run `npm run build`. You can then run the bot using `node ./dist/main.js`
