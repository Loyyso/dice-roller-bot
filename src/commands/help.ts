import Command from '../types/command';
import BotClient from '../types/bot-client';
import {MessageEmbed} from 'discord.js';


const helpCommand: Command = {
  commandData: {
    name: 'help',
    description: 'Prints help.',
    options: [{
      name: 'name',
      description: 'The command\'s name.',
      type: 3,
      required: false,
    }],
  },
  exec: async (interaction) => {
    const embed = new MessageEmbed();

    const {commands} = interaction.client as BotClient;

    if (interaction.options.length === 0) {
      embed.setColor('#AA0000')
          .setTitle('Help page')
          .setDescription('List of all commands')
          .addField('Commands',
              commands.map((command) =>
                `**${command.commandData.name}**`).join(', '),
          )
          .setFooter(`You can send "/help [command name]" to get ` +
      `info on a specific command!`)
          .setTimestamp();

      try {
        interaction.user.send(embed);
        if (interaction.channel?.type === 'dm') return;
        interaction.reply('You have recieved a DM with all the commands.');
      } catch (error) {
        console.error(`Could not send help DM to ${interaction.user.tag}.\n`,
            error);
        interaction.reply('It seems like we cannot send you a DM.' +
          'Do you have DMs disabled?');
      }
    } else {
      const name = interaction.options[0].value?.toString().toLowerCase() || '';
      const command = commands.get(name);

      if (!command) {
        return interaction.reply('That\'s not a valid command.');
      }

      embed.setColor('#0000AA')
          .setTitle(command.commandData.name)
          .setDescription(command.commandData.description);

      if (command.commandData.options) {
        const usage = command.commandData.options.map(
            (option) => option.name,
        ).join(', ');
        embed.addField('Usage', `\`/${command.commandData.name} ${usage}\``);
      }

      embed.setTimestamp();

      interaction.reply(embed);
    }
  },
};

export default helpCommand;
