import Command from '../types/command';

const pingCommand: Command = {
  commandData: {
    name: 'ping',
    description: 'Returns "Pong!".',
  },
  exec: (interaction) => {
    interaction.reply('Pong!');
  },
};

export default pingCommand;
