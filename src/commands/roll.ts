import {randomInt} from 'crypto';
import {MessageEmbed} from 'discord.js';
import {evaluate} from 'mathjs';
import Command from '../types/command';

const rollCommand: Command = {
  commandData: {
    name: 'roll',
    description: 'Roll dices!',
    options: [{
      name: 'expression',
      description: 'The expression to process.',
      required: true,
      type: 3,
    }],
  },
  exec: (interaction) => {
    let expression = interaction.options[0].value as string;

    // process dices formatted as 'nb d nb'
    const regexp1 = /\d+d\d+/;
    while (expression.search(regexp1) !== -1) {
      const matches = expression.match(regexp1) as RegExpMatchArray;
      const [nb, sides] = matches[0].split('d');
      const value = computeValue(parseInt(nb), parseInt(sides));
      expression = expression.replace(regexp1, value.toString());
    }

    // process dices formatted as 'd nb'
    const regexp2 = /d\d+/;
    while (expression.search(regexp2) !== -1) {
      const matches = expression.match(regexp2) as RegExpMatchArray;
      const sides = matches[0].split('d')[1];
      const value = computeValue(1, parseInt(sides));
      expression = expression.replace(regexp2, value.toString());
    }

    // remove whitespace
    expression = expression.replace(/\s+/g, '');

    const embed = new MessageEmbed()
        .addFields(
            {name: 'Result', value: expression},
            {name: 'Total', value: evaluate(expression)},
        );

    interaction.reply(embed);
  },
};

/**
 * Rolls `nb` `sides`-sided dices and returns the computed value.
 * @param {number} nb The number of dices to roll.
 * @param {number} sides The number of sides the dice has.
 * @return {number} The computed value.
 */
function computeValue(nb: number, sides: number): number {
  let value = 0;
  for (let i=0; i<nb; i++) {
    value += randomInt(1, sides+1);
  }
  return value;
}

export default rollCommand;
