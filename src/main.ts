import {exit} from 'process';
import config from './config.json';
import Command from './types/command';
import BotClient from './types/bot-client';
import {findFilesRecursively} from './util/utils';
import {join} from 'path';

/**
 * Entry point
 */
function main() {
  const client = new BotClient({
    intents: ['GUILDS', 'GUILD_MESSAGES', 'DIRECT_MESSAGES'],
  });

  const commandList = findFilesRecursively(join(__dirname, 'commands'))
      .filter((file) => file.endsWith('.ts') || file.endsWith('.js'));
  for (const file of commandList) {
    const command: Command = module.require(file).default;
    client.commands.set(command.commandData.name, command);
  }

  client.on('ready', () => {
    console.log(`Logged in as ${client.user?.tag}.`);

    for (const command of client.commands.values()) {
      client.application?.commands.create(command.commandData);
    }
  });

  client.on('interaction', (interaction) => {
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);
    try {
      if (!command) throw new Error('Command not recognized.');
      command.exec(interaction);
    } catch (error) {
      console.error(error);
      interaction.reply('An error occured while processing your command.');
    }
  });

  try {
    client.login(config.token);
  } catch (error) {
    console.error('Invalid token.');
    exit(-1);
  }
}

main();
