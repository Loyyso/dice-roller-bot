import {Client, ClientOptions, Collection} from 'discord.js';
import Command from './command';

/**
 * Extension of discord.js Client with a command collection
 */
class BotClient extends Client {
  commands: Collection<string, Command>;

  /**
   * Instantiate the attributes
   * @param {ClientOptions} options Options for the client .
   */
  constructor(options: ClientOptions) {
    super(options);
    this.commands = new Collection();
  }
}

export default BotClient;
