import {ApplicationCommandData, CommandInteraction} from 'discord.js';

interface Command {
  commandData: ApplicationCommandData,
  exec: (interaction: CommandInteraction) => void,
}

export default Command;
