import {readdirSync, statSync} from 'fs';
import {join} from 'path';

/**
 * Finds all files in a given directory.
 * @param {string} path The path to the hierarchy's root directory.
 * @return {string[]} All files in the hierarchy.
 */
export function findFilesRecursively(path: string): string[] {
  if (statSync(path).isFile()) return [path];

  const files: string[] = [];
  const dirs: string[] = [];

  const elements = readdirSync(path);

  for (const element of elements) {
    const elementStats = statSync(join(path, element));
    if (elementStats.isFile()) {
      files.push(join(path, element));
    } else if (elementStats.isDirectory()) {
      dirs.push(element);
    }
  }

  for (const dir of dirs) {
    files.push(...findFilesRecursively(join(path, dir)));
  }

  return files;
}
